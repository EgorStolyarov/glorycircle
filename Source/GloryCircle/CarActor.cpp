#include "CarActor.h"
#include "BombActor.h"
#include "Components/SphereComponent.h"
#include "Particles/ParticleSystemComponent.h"

// Sets default values
ACarActor::ACarActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionComponent"));
	RootComponent = CollisionComponent;

	VisibleBombComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BombComponent"));
	VisibleBombComponent->SetupAttachment(RootComponent);

	ParticleComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleComponent"));
	ParticleComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ACarActor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACarActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (AxisRight)
	{
		FRotator Rotation = FRotator(0, AxisRight * RotateSpeed * DeltaTime, 0);
		AddActorWorldRotation(Rotation);
	}

	if (CurrentSpeed) {
		FVector Location = GetActorForwardVector() * CurrentSpeed * DeltaTime;
		AddActorWorldOffset(Location, true);
	}

	if ((AxisForward > 0.0f) && bAllowMovement)
	{
		CurrentSpeed += SpeedAccelleration * DeltaTime;
		if (CurrentSpeed > SpeedMaxValue) CurrentSpeed = SpeedMaxValue;
	}
	else if (CurrentSpeed)
	{
		CurrentSpeed -= SpeedLowerage * DeltaTime;
		if (CurrentSpeed < 0.0f) CurrentSpeed = 0.0f;
	}
}

void ACarActor::SufferCrash(){
	bAllowMovement = false;
	ParticleComponent->ActivateSystem();
	GetWorld()->GetTimerManager().SetTimer(EnableMovementTimerHandle, this, &ACarActor::EnableMovement, BombMovementDelay, true);
}

void ACarActor::PlantBomb()
{
	if (bAllowPlant)
	{
		FVector SpawnLocation = GetActorLocation();
		GetWorld()->SpawnActor<ABombActor>(BombClass, FTransform(SpawnLocation));

		bAllowPlant = false;
		VisibleBombComponent->SetVisibility(false);

		GetWorld()->GetTimerManager().SetTimer(EnablePlantTimerHandle, this, &ACarActor::EnablePlant, BombDropDelay, true);
	}	
}

void ACarActor::EnableMovement(){
	bAllowMovement = true;
	ParticleComponent->DeactivateSystem();
}

void ACarActor::EnablePlant(){
	bAllowPlant = true;
	VisibleBombComponent->SetVisibility(true);
}


