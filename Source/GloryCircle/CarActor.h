// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CarActor.generated.h"

class UStaticMeshComponent;
class USphereComponent;
class UParticleSystemComponent;
class ABombActor;

UCLASS()
class GLORYCIRCLE_API ACarActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		USphereComponent* CollisionComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* VisibleBombComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UParticleSystemComponent* ParticleComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Choose Bomb Class")
		TSubclassOf<ABombActor> BombClass;	

	UPROPERTY(EditDefaultsOnly, Category = "Bomb")
		float BombMovementDelay = 2.2f;
	UPROPERTY(EditDefaultsOnly, Category = "Bomb")
		float BombDropDelay = 3.1f;	

	UPROPERTY(EditDefaultsOnly, Category = "Movement")
		float SpeedAccelleration = 1200.0f;
	UPROPERTY(EditDefaultsOnly, Category = "Movement")
		float SpeedLowerage = 1800.0f;
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Movement")
		float SpeedMaxValue = 2100.0f;
	UPROPERTY(EditDefaultsOnly, Category = "Movement")
		float RotateSpeed = 180.0f;	

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
	float AxisForward = 0.0f;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
	float AxisRight = 0.0f;

	float CurrentSpeed = 0.0f;
	bool bAllowMovement = true;
	bool bAllowPlant = true;

	FTimerHandle EnableMovementTimerHandle;
	FTimerHandle EnablePlantTimerHandle;

	ACarActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION()
	void SufferCrash();
	UFUNCTION(BlueprintCallable)
	void PlantBomb();

	UFUNCTION()
	void EnableMovement();
	UFUNCTION()
	void EnablePlant();

	
};
