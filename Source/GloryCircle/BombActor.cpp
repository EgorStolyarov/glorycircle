#include "BombActor.h"
#include "CarActor.h"
#include "Components/StaticMeshComponent.h"


// Sets default values
ABombActor::ABombActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	RootComponent = VisibleComponent;	
}

// Called when the game starts or when spawned
void ABombActor::BeginPlay()
{
	Super::BeginPlay();

	VisibleComponent->OnComponentEndOverlap.AddDynamic(this, &ABombActor::OnOverlapEnd); 
}

// Called every frame
void ABombActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABombActor::OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (!isReady)
	{
		isReady = true;
		VisibleComponent->OnComponentBeginOverlap.AddDynamic(this, &ABombActor::OnOverlapBegin); 
	} 
}

void ABombActor::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	auto Car = Cast<ACarActor>(OtherActor);
	if (IsValid(Car))
	{
		Car->SufferCrash();
		Destroy();
	}	
}



